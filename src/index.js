import "./styles/index.scss";

import {startCam} from "./js/webcam";
import * as faceapi from "face-api.js";
import AuthorizedPerson from "./js/models/AuthorizedPerson";

const MODELS_URL = '/assets/fd-models';
const statusDiv = document.getElementById("match-result");
const initData = async () => {
    statusDiv.innerText = "Loading models...";
    await faceapi.loadFaceDetectionModel(MODELS_URL);
    await faceapi.loadFaceRecognitionModel(MODELS_URL);
    await faceapi.loadSsdMobilenetv1Model(MODELS_URL);
    await faceapi.loadFaceLandmarkModel(MODELS_URL);
};

const compareFaces = async descriptors => {
    if (!descriptors || !descriptors.length) {
        return;
    }
    statusDiv.innerText = "Initializing...";
    await startCam();
    const video = document.getElementById("camera-feed");
    video.addEventListener('play', () => {
        const canvas = faceapi.createCanvasFromMedia(video);
        document.body.appendChild(canvas);
        const cameraDimensions = { width: video.clientWidth, height: video.clientHeight };
        faceapi.matchDimensions(canvas, cameraDimensions);
        const faceMatcher = new faceapi.FaceMatcher(descriptors);
        statusDiv.remove();
        setInterval(async () => {
            const results = await faceapi
                .detectAllFaces(video)
                .withFaceLandmarks()
                .withFaceDescriptors();
            if (results && results.length) {
                canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
                faceapi.draw.drawFaceLandmarks(canvas, faceapi.resizeResults(results, cameraDimensions));
                for (const fd of results) {
                    const match = faceMatcher.findBestMatch(fd.descriptor);
                    if (match && match.distance < 0.6) {
                        (new faceapi.draw.DrawBox(fd.detection.box, {label: [match.label, `Distance: ${match.distance.toFixed(3)}`]})).draw(canvas);
                        continue;
                    }
                    (new faceapi.draw.DrawBox(fd.detection.box, {label: "Unknown person", boxColor: "red"})).draw(canvas);
                }
                return;
            }
            canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
        }, 50);
    });
};


initData()
    .then(() => {compareFaces(AuthorizedPerson.generateLabeledFaceDescriptors(AuthorizedPerson.getDummyData()))});
